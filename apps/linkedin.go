// Copyright 2014 beego authors
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.
//
// Maintain by https://github.com/slene

package apps

import (
	"github.com/beego/social-auth"
)

type Linkedin struct {
	BaseProvider
}

func (p *Linkedin) GetType() social.SocialType {
	return social.SocialLinkedin
}

func (p *Linkedin) GetName() string {
	return "Linkedin"
}

func (p *Linkedin) GetPath() string {
	return "linkedin"
}

func (p *Linkedin) GetIndentify(tok *social.Token) (string, error) {
	return tok.GetExtra("uid"), nil
}

var _ social.Provider = new(Linkedin)

func NewLinkedin(clientId, secret string) *Linkedin {
	p := new(Linkedin)
	p.App = p
	p.ClientId = clientId
	p.ClientSecret = secret
	p.ResponseType = "code"
	p.State = "DCEeFWfad23dfKef424"
	p.Scope = ""
	p.AuthURL = "https://www.linkedin.com/oauth/v2/authorization"
	p.TokenURL = "https://www.linkedin.com/oauth/v2/accessToken"
	p.RedirectURL = social.DefaultAppUrl + "login/linkedin/access"
	p.AccessType = ""
	p.ApprovalPrompt = ""
	return p
}
